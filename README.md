# Madillustration
A simplified version of madanalysis to illustrate how the madanalysis pipeline works.

## Read (read_bagfiles.py)
Reads in the raw data for each experiment(bagfile) to several (sub)dataframes

## Aggregation and pre-computation (prepare.py)
Merges all of these (sub)dataframes to one dataframe per experiment and resamples. It then adds the metadata from the filenames and computes values which are necessary for all experiments (e.g scaled coordinates, scaled and smoothed velocity etc.)

Both read_bagfile.py and prepare.py save caches to /raw_caches and /caches respectively Throughout the first two steps all of the data is completely independent from one another:

one bagfile -> one raw_cache file -> one cache file

Thus we can distribute the data across many processes running side by side. Effectively this means that for this computationally expensive part of the pipeline you can get a great speed up on multicore machines.

Optionally you can use data_info.py to obtain files to check the integrity and some assumptions about the data before storing them in the hdfstore

## Store (store_data.py)
Stores all of the dataframes (one per experiment) in a big hdfstore.

## Selection and plotting
The benefit of having one big store containing all experiments is that you now can flexibly select experiments depending on metadata and plot them toward each other. For an example on how to select from and use this big store see example_plots.ipynb (http://nbviewer.jupyter.org/gist/hdorothea/277a3e699d287220ed75077f08678430)
