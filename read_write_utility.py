import cPickle as pickle
import fnmatch
import os.path


def load_cache(cache_fname):
    if os.path.exists(cache_fname):
        print 'loading cache', cache_fname
        cache_buf = open(cache_fname, 'rb')
        try:
            cache_dict = pickle.load(cache_buf)
        except Exception as err:
            print 'loading cache %s failed\n\t%s' % (cache_fname, err, )
        else:
            results = cache_dict['results']
            # print '\tloaded cache succeeded'
            return results

    return None


def save_cache(results, cache_fname):
    cache_dict = {}
    cache_dict['results'] = results
    pickle.dump(cache_dict, open(cache_fname, 'wb'), -1)
    print 'saved cache', cache_fname


def filter_bagpaths_without_cache(bagpaths):
    bagpaths_without_cache = []
    for bagpath in bagpaths:
        if not os.path.isfile(get_cache_path(bagpath, 'cache')):
            bagpaths_without_cache.append(bagpath)

    return bagpaths_without_cache


def get_cache_path(bagpath, name):
    caches_directory_path = os.path.join(
        os.path.dirname(bagpath), '{}s'.format(name))

    cache_path = os.path.join(caches_directory_path,
                              os.path.basename(bagpath) + '.{}'.format(name))

    return cache_path


def make_cache_dirs(bagpaths, name):
    if not isinstance(bagpaths, (list, tuple)):
        bagpaths = [bagpaths]

    for directory in set(map(os.path.dirname, bagpaths)):
        caches_directory_path = os.path.join(directory, name)
        if not os.path.isdir(caches_directory_path):
            os.makedirs(caches_directory_path)


def get_bagpaths(path):
    bagpaths = []
    for root, dirnames, filenames in os.walk(path):
        dirnames[:] = [d for d in dirnames if d not in ['caches', 'raw_caches']]
        for filename in fnmatch.filter(filenames, '*.bag'):
            bagpaths.append(os.path.join(root, filename))

    return bagpaths
