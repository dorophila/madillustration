import argparse
import datetime
import multiprocessing
import os

import numpy as np
import pandas as pd
import pytz
import rosbag
import roslib
import src.laser_camera_calibration as laser_camera_calibration
from read_write_utility import (get_bagpaths, load_cache, make_cache_dirs,
                                save_cache)

roslib.load_manifest('flymad')


def get_arena_dict(bagpath):
    # scalled values are in mm, raw values are in px
    arena_dict = {'cx': 0.0, 'cy': 0.0, 'r': 45.0}
    calibration = laser_camera_calibration.load_calibration(bagpath)
    bounds_raw, xlim_raw, ylim_raw = calibration.get_arena_measurements()
    arena_dict['cx_raw'], arena_dict[
        'cy_raw'], arena_dict['r_raw'] = bounds_raw
    arena_dict['sx'] = 45.0 / ((xlim_raw[1] - xlim_raw[0]) / 2.0)
    arena_dict['sy'] = 45.0 / ((ylim_raw[1] - ylim_raw[0]) / 2.0)
    return arena_dict


def read_bagfile(bagpath, tzname=None):
    '''Read the raw data from the bagfile into pandas dataframes.
    Each rostopic gets its own dataframe (l_df, t_df, h_df, r_df) and one arena
    dictionary'''

    raw_caches_directory_path = os.path.join(
        os.path.dirname(bagpath), 'raw_caches')

    cache_path_name = os.path.join(raw_caches_directory_path,
                                   os.path.basename(bagpath) + '.raw-cache')

    results = load_cache(cache_path_name)
    if results is not None:
        return results

    print "reading", bagpath
    bag = rosbag.Bag(bagpath)

    if tzname is None:
        tzname = 'CET'

    tz = pytz.timezone(tzname)

    arena_dict = get_arena_dict(bagpath)

    l_index = []
    l_data = {k: [] for k in ("laser_power", "mode")}

    t_index = []
    t_data = {k: [] for k in ("obj_id", "x_raw", "y_raw", "theta")}

    h_index = []

    h_data = {k: []
              for k in ("body_x", "body_y", "target_x", "target_y",
                        "contour_area", "h_theta")}

    h_data_names = ("body_x", "body_y", "target_x", "target_y", "contour_area")

    geom = None

    topics = ["/targeter/targeted", "/flymad/tracked", "/draw_geom/poly",
              "/flymad/laser_head_delta", "/flymad/raw_2d_positions"]

    study = os.path.basename(bagpath).split('.')[0].split('_')[0]

    for topic, msg, rostime in bag.read_messages(topics=topics):
        if topic == "/targeter/targeted":
            ts = msg.header.stamp.to_sec()
            aware_ts = datetime.datetime.fromtimestamp(ts, tz)
            l_index.append(aware_ts)
            l_data['laser_power'].append(msg.laser_power)
            l_data['mode'].append(msg.mode)
        elif topic == "/flymad/tracked":
            if msg.is_living:
                ts = msg.header.stamp.to_sec()
                aware_ts = datetime.datetime.fromtimestamp(ts, tz)
                t_index.append(aware_ts)
                t_data['obj_id'].append(msg.obj_id)
                t_data['x_raw'].append(msg.state_vec[0])
                t_data['y_raw'].append(msg.state_vec[1])
                t_data['theta'].append(msg.theta_passthrough)
        elif topic == "/flymad/laser_head_delta":
            ts = rostime.to_sec()
            aware_ts = datetime.datetime.fromtimestamp(ts, tz)
            for k in h_data_names:
                h_data[k].append(getattr(msg, k, np.nan))
            h_data["h_theta"].append(getattr(msg, 'theta', np.nan))
            h_index.append(aware_ts)
        elif topic == "/draw_geom/poly":
            # we are only interested in the first geom message
            # and that only if the study was a tile study
            if not geom and 'position' in study:
                raw_points_x = [pt.x for pt in msg.points]
                raw_points_y = [pt.y for pt in msg.points]
                geom = zip(raw_points_x, raw_points_y)

    # position experiemnts without draw_geom message
    # fall back to standard geom
    if 'position' in study and 'post' not in study and not geom:
        geom = [(100.0, 300.0), (100.0, 500.0), (300.0, 500.0),
                (300.0, 300.0), (100.0, 300.0)]

    l_df = pd.DataFrame(l_data, index=l_index, dtype='float64')
    t_df = pd.DataFrame(t_data, index=t_index, dtype='float64')
    h_df = pd.DataFrame(h_data, index=h_index, dtype='float64')
    results = (l_df, t_df, h_df, arena_dict, geom)

    # save raw-cache
    save_cache(results, cache_path_name)

    return results


def read_multi(bagpaths):
    make_cache_dirs(bagpaths, 'raw_caches')
    p = multiprocessing.Pool()
    p.map(read_bagfile, bagpaths)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', help='path to data folder')

    args = parser.parse_args()
    path = args.data

    if os.path.isdir(path):
        bagpaths = get_bagpaths(path)
    else:
        bagpaths = [path]

    read_multi(bagpaths)
