import datetime
import os
import sys
import unittest


from madanalysis.read_bagfile import read_bagfile

sys.path.append(os.path.join(sys.path[0], '..'))



class ReadTestCase(unittest.TestCase):

    # test bagpaths

    _stimulation_bagpath = "test_data/3+1+3x2_ctrl_cs_980nm136mAmps100mm+uvbacklight382mAmps_4days_fake_2016-02-04-13-02-15.bag"

    _laser_always_on_stimulation_bagpath = "test_data/0+11+0x1_ctrl_cs_980nm136mAmps100mm+uvbacklight382mAmps_4days_fake_2016-02-16-16-48-58.bag"

    _laser_never_on_stimulation_bagpath = "test_data/11+0+0x1_ctrl_cs_980nm136mAmps100mm+uvbacklight382mAmps_4days_fake_2016-02-16-16-51-27.bag"

    _stimulation_with_geom_on_bagpath = "test_data/3+1+3x2_ctrl_cs_980nm136mAmps100mm+uvbacklight382mAmps_4days_fake0-250-338-494tileon_2016-02-16-16-41-24.bag"

    _geom_down_bagpath = "test_data/position11_ctrl_cs_980nm136mAmps100mm+uvbacklight382mAmps_4days_fakeflyintile0-250-338-494_2016-02-16-17-14-42.bag"

    _geom_up_bagpath = "test_data/position11_ctrl_cs_980nm136mAmps100mm+uvbacklight382mAmps_4days_fakeflyintile338-0-659-250_2016-02-16-17-34-31.bag"

    def read_bagfile(self, _bagpath):
        bagpath = os.path.join(sys.path[0], _bagpath)
        l_df, t_df, h_df, arena_dict, geom = read_bagfile(bagpath)
        return l_df, t_df, h_df, arena_dict, geom

    def test_l_df_columns_stimulation_bagpath_exist(self):
        l_df = self.read_bagfile(self.__class__._stimulation_bagpath)[0]
        # this would fail if the topic was simply not saved
        columns = ['laser_power', 'mode']
        self.assertTrue(all(column in l_df.columns for column in columns))

    def test_laser_power_stimulation_bagpath_between_0_and_255(self):
        l_df = self.read_bagfile(self.__class__._stimulation_bagpath)[0]
        self.assertTrue((l_df['laser_power'].min() >= 0) and (l_df[
            'laser_power'].max() <= 25))

    def test_laser_power_laser_never_on_bagpath_0(self):
        l_df = self.read_bagfile(
            self.__class__._laser_never_on_stimulation_bagpath)[0]
        self.assertTrue((l_df['laser_power'].max() == 0) and (l_df[
            'laser_power'].min() == 0))

    def test_mode_stimulation_bagpath_always_between_0_and_2(self):
        # body, uebergang, head
        l_df = self.read_bagfile(self.__class__._stimulation_bagpath)[0]
        self.assertTrue((l_df['laser_power'].min() >= 0) and (l_df[
            'laser_power'].max() <= 2))

    def test_t_df_columns_stimulation_bagpath_exist(self):
        # this would fail if the topic was simply not saved
        t_df = self.read_bagfile(self.__class__._stimulation_bagpath)[1]
        columns = ['obj_id', 'theta', 'x_raw', 'y_raw']
        self.assertTrue(all(column in t_df.columns for column in columns))

    def test_h_df_columns_stimulation_bagpath_exist(self):
        # this would fail if the topic was simply not saved
        h_df = self.read_bagfile(self.__class__._stimulation_bagpath)[2]
        columns = ["body_x", "body_y", "target_x", "target_y", "contour_area",
                   "h_theta"]
        self.assertTrue(all(column in h_df.columns for column in columns))

    def test_geom_stimulation_bagpath_None(self):
        geom = self.read_bagfile(self.__class__._stimulation_bagpath)[-1]
        self.assertIsNone(geom)

    def test_geom_stimulation_with_geom_on_bagpath_None(self):
        geom = self.read_bagfile(
            self.__class__._stimulation_with_geom_on_bagpath)[-1]
        self.assertIsNone(geom)

    def test_geom_geom_down_bagpath_exists(self):
        geom = self.read_bagfile(self.__class__._geom_down_bagpath)[-1]
        self.assertTrue(geom)

    def test_geom_geom_up_bagpath_exists(self):
        geom = self.read_bagfile(self.__class__._geom_up_bagpath)[-1]
        self.assertTrue(geom)

    def test_geom_geom_up_bagpath_geom_down_bagpath_differ(self):
        geom_up = self.read_bagfile(self.__class__._geom_up_bagpath)[-1]
        geom_down = self.read_bagfile(self.__class__._geom_down_bagpath)[-1]
        self.assertTrue(geom_up != geom_down)

    def test_geom_none_stimulation_bagpath_with_geom(self):
        geom = self.read_bagfile(
            self.__class__._stimulation_with_geom_on_bagpath)[-1]
        self.assertIsNone(geom)

    def test_raw_coordinates_inside_raw_arena(self):

        def inside_arena(row):
            return ((arena_dict['cx_raw'] - row['x_raw'])**2 +
                    (arena_dict['cy_raw'] - row['y_raw'])**2 <
                    arena_dict['r_raw']**2)

        results = self.read_bagfile(self.__class__._stimulation_bagpath)
        t_df = results[1]
        arena_dict = results[3]
        in_arena = t_df.loc[:, ['x_raw', 'y_raw']].apply(inside_arena, axis=1)
        self.assertTrue(in_arena.all())


if __name__ == '__main__':
    unittest.main()
