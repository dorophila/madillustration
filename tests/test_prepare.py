import os
import pandas as pd
import sys
import unittest

sys.path.append(os.path.join(sys.path[0], '..'))

import prepare_utility
from read_bagfile import read_bagfile

from datetime import datetime


class PrepareTestCase(unittest.TestCase):

    _stimulation_bagpath = "test_data/3+1+3x2_ctrl_cs_980nm136mAmps100mm+uvbacklight382mAmps_4days_fake_2016-02-04-13-02-15.bag"

    _10_points_tracked_at_beginning_stimulation_bagpath = "test_data/3+1+3x2_ctrl_cs_980nm136mAmps100mm+uvbacklight382mAmps_4days_fake10pointstrackedatbeginning_2016-02-16-16-56-56.bag"

    _laser_always_on_stimulation_bagpath = "test_data/0+11+0x1_ctrl_cs_980nm136mAmps100mm+uvbacklight382mAmps_4days_fake_2016-02-16-16-48-58.bag"

    _laser_always_off_stimulation_bagpath = "test_data/11+0+0x1_ctrl_cs_980nm136mAmps100mm+uvbacklight382mAmps_4days_fake_2016-02-16-16-51-27.bag"

    _geom_down_fly_in_tile_bagpath = "test_data/position11_ctrl_cs_980nm136mAmps100mm+uvbacklight382mAmps_4days_fakeflyintile0-250-338-494_2016-02-16-17-14-42.bag"

    _geom_up_fly_in_tile_bagpath = "test_data/position11_ctrl_cs_980nm136mAmps100mm+uvbacklight382mAmps_4days_fakeflyintile338-0-659-250_2016-02-16-17-34-31.bag"

    # then in and out
    _geom_up_fly_in_tile_at_beginning_bagpath = "test_data/position11_ctrl_cs_980nm136mAmps100mm+uvbacklight382mAmps_4days_fakeflyintile338-0-659-250atbeginning_2016-02-16-17-30-20.bag"

    def read_bagfile(self, _bagpath):
        bagpath = os.path.join(sys.path[0], _bagpath)
        l_df, t_df, h_df, arena_dict, geom = read_bagfile(bagpath)
        return l_df, t_df, h_df, arena_dict, geom

    def test_add_meta_data_columns_meta_data_columns_present(self):
        bagpath = self.__class__._stimulation_bagpath
        l_df = self.read_bagfile(bagpath)[0]
        l_df = prepare_utility.add_meta_data_columns(l_df, bagpath)
        meta_data_columns = ['study', 'binary_genotype',
                             'genotype', 'flymad_setting', 'age', 'info', 'start_string', 'start_time']
        self.assertTrue(all([column_name in l_df.columns for column_name in meta_data_columns]))

    def test_add_meta_data_columns_meta_data_columns_the_expected_thing(self):
        bagpath = self.__class__._stimulation_bagpath
        l_df = self.read_bagfile(bagpath)[0]
        l_df = prepare_utility.add_meta_data_columns(l_df, bagpath)
        self.assertEqual(l_df.at[l_df['study'].first_valid_index(), 'study'], '3+1+3x2')
        self.assertEqual(l_df.at[l_df['binary_genotype'].first_valid_index(), 'binary_genotype'], 'ctrl')
        self.assertEqual(l_df.at[l_df['genotype'].first_valid_index(), 'genotype'], 'cs')
        self.assertEqual(l_df.at[l_df['flymad_setting'].first_valid_index(), 'flymad_setting'], '980nm136mAmps100mm+uvbacklight382mAmps')
        self.assertEqual(l_df.at[l_df['age'].first_valid_index(), 'age'], 4)
        self.assertEqual(l_df.at[l_df['info'].first_valid_index(), 'info'], 'fake')
        self.assertEqual(l_df.at[l_df['start_string'].first_valid_index(), 'start_string'], '2016-02-04-13-02-15')
        self.assertEqual(l_df.at[l_df['start_time'].first_valid_index(), 'start_time'], datetime(year=2016, month=2, day=4, hour=13, minute=2, second=15))

    def test_add_section_column_laser_always_off(self):
        results = self.read_bagfile(self.__class__._laser_always_off_stimulation_bagpath)
        l_df = results[0]
        geom = results[4]
        l_df = prepare_utility.add_section_column(l_df, geom)
        self.assertEqual(l_df['section'].max(), 0)

    def test_add_laser_change_column_laser_always_off(self):
        l_df = self.read_bagfile(self.__class__._laser_always_off_stimulation_bagpath)[0]
        l_df = prepare_utility.add_laser_change_column(l_df)
        self.assertTrue(l_df['laser_change'].apply(lambda x: x == 0).all())

    def test_add_section_column_laser_always_on_1_section_from_beginning_to_end(self):
        results = self.read_bagfile(self.__class__._laser_always_on_stimulation_bagpath)
        l_df = results[0]
        geom = results[4]
        l_df = prepare_utility.add_section_column(l_df, geom)
        self.assertEqual(l_df['section'].max(), 0)

    def test_add_section_laser_always_on_sliced_first_last_section_value_0(self):
        results = self.read_bagfile(self.__class__._laser_always_on_stimulation_bagpath)
        l_df = results[0]
        geom = results[4]
        l_df = l_df.loc[:l_df.last_valid_index() - pd.Timedelta(seconds=4), ]
        l_df = prepare_utility.add_section_column(l_df, geom)
        self.assertEqual(l_df.ix[0, 'section'], 0)
        self.assertEqual(l_df.ix[len(l_df)-1, 'section'], 0)

    def test_add_section_column_None_for_geom_bagpath(self):
        # I have an extra conditional for this to avoid useless calculation
        # still in any case the section column of geom bagpaths should be set
        # to None because their laser_ons are not of equal length
        results = self.read_bagfile(
            self.__class__._geom_up_fly_in_tile_at_beginning_bagpath)
        l_df = results[0]
        geom = results[4]
        l_df = prepare_utility.add_section_column(l_df, geom)
        self.assertTrue(l_df['section'].isnull().all())

    def test_add_laser_state_column_laser_always_1(self):
        # problem is I start to record before starting the experiment node
        # and stop recording after stoping the experiment node
        l_df, t_df, h_df, arena_dict, geom = self.read_bagfile(
            self.__class__._laser_always_on_stimulation_bagpath)
        df = pd.concat([l_df, t_df], axis=1)
        df = df.loc[:df.last_valid_index() - pd.Timedelta(seconds=4), :]
        df = prepare_utility.add_laser_state_column(df)
        df = prepare_utility.switch_to_timedelta_index(df)
        self.assertTrue(df['laser_state'].dropna().apply(lambda x: x == 1).all())

    def test_add_laser_state_column_laser_always_0(self):
        l_df = self.read_bagfile(
            self.__class__._laser_always_off_stimulation_bagpath)[0]
        l_df = prepare_utility.add_laser_state_column(l_df)

        boolean_laser_state = l_df['laser_state'].apply(lambda x: x == 0)
        self.assertTrue(boolean_laser_state.all())

    def test_10_points_at_beginning_filter_short_fewer_obj_ids(self):
        t_df = self.read_bagfile(
            self.__class__._10_points_tracked_at_beginning_stimulation_bagpath)[1]
        pre_filter_num_obj_id = len(t_df['obj_id'].unique())
        t_df = prepare_utility.filter_short_tracking(t_df)
        post_filter_num_obj_id = len(t_df['obj_id'].unique())
        self.assertGreater(pre_filter_num_obj_id, post_filter_num_obj_id)

    def test_add_scaled_coordinates_inside_scaled_arena(self):
        results = self.read_bagfile(self.__class__._stimulation_bagpath)
        _t_df = results[1]
        arena_dict = results[3]
        t_df = prepare_utility.add_scaled_coordinates_columns(_t_df, arena_dict)
        in_arena = t_df.loc[:, ['x', 'y']].apply(lambda x: (arena_dict[
                                                 'cx'] - x['x'])**2 + (arena_dict['cy'] - x['y'])**2 < arena_dict['r']**2, axis=1)
        self.assertTrue(in_arena.all())

    def test_add_in_area_fly_always_in_area_geom_down(self):
        results = self.read_bagfile(
            self.__class__._geom_down_fly_in_tile_bagpath)
        t_df = results[1]
        arena_dict = results[3]
        geom = results[4]
        t_df = prepare_utility.add_in_area_column(t_df, arena_dict, geom)
        self.assertTrue(t_df['in_area'].all())

    def test_add_in_area_fly_always_in_area_geom_up(self):
        results = self.read_bagfile(
            self.__class__._geom_up_fly_in_tile_bagpath)
        t_df = results[1]
        arena_dict = results[3]

        geom = results[4]
        t_df = prepare_utility.add_in_area_column(t_df, arena_dict, geom)
        self.assertTrue(t_df['in_area'].all())

    def test_add_in_area_fly_not_in_opposite_geom(self):
        results_up = self.read_bagfile(
            self.__class__._geom_up_fly_in_tile_bagpath)
        results_down = self.read_bagfile(
            self.__class__._geom_down_fly_in_tile_bagpath)
        t_df = results_up[1]
        arena_dict = results_up[3]

        geom = results_down[4]
        t_df = prepare_utility.add_in_area_column(t_df, arena_dict, geom)
        self.assertFalse(t_df['in_area'].any())

    def test_add_in_area_fly_in_geom_at_beginning_but_not_allways(self):
        results = self.read_bagfile(
            self.__class__._geom_up_fly_in_tile_at_beginning_bagpath)
        t_df = results[1]
        arena_dict = results[3]
        geom = results[4]
        t_df = prepare_utility.add_in_area_column(t_df, arena_dict, geom)
        self.assertTrue(t_df.loc[t_df.first_valid_index(), 'in_area'])
        self.assertFalse(t_df['in_area'].all())

    def test_add_laser_change_column_are_there_two_laser_ons(self):
        l_df = prepare_utility.add_laser_change_column(
            self.read_bagfile(self.__class__._stimulation_bagpath)[0])
        num_laser_ons = len(l_df['laser_change'][l_df['laser_change'] == 1])

        self.assertEqual(num_laser_ons, 2)

    def test_add_laser_change_column_are_there_two_laser_offs(self):
        l_df = prepare_utility.add_laser_change_column(
            self.read_bagfile(self.__class__._stimulation_bagpath)[0])
        num_laser_offs = len(l_df['laser_change'][l_df['laser_change'] == -1])

        self.assertEqual(num_laser_offs, 2)

    def test_add_laser_change_laser_always_on_no_slicing_1_laser_on_1_laser_off(self):
        l_df = self.read_bagfile(
            self.__class__._laser_always_on_stimulation_bagpath)[0]
        l_df = prepare_utility.add_laser_change_column(l_df)
        num_laser_on = len(l_df.loc[l_df['laser_change'] == 1, 'laser_change'])
        num_laser_off = len(l_df.loc[l_df['laser_change'] == -1, 'laser_change'])
        self.assertEqual(num_laser_on, 1)
        self.assertEqual(num_laser_off, 1)

    def test_add_laser_change_laser_always_on_laser_on_at_beginning_laser_off_at_end(self):
        l_df = self.read_bagfile(
            self.__class__._laser_always_on_stimulation_bagpath)[0]
        # end of experiment node sends laser off
        # this way we should only have laser on
        l_df = l_df.loc[:l_df.last_valid_index() - pd.Timedelta(seconds=4), :]
        l_df = prepare_utility.add_laser_change_column(l_df)
        self.assertEqual(l_df.loc[l_df.first_valid_index(), 'laser_change'], 1)
        self.assertEqual(l_df.loc[l_df.last_valid_index(), 'laser_change'], -1)

    def test_add_laser_change_laser_always_off_laser_change_always_0(self):
        l_df = self.read_bagfile(
            self.__class__._laser_always_off_stimulation_bagpath)[0]
        l_df = prepare_utility.add_laser_change_column(l_df)
        self.assertTrue(l_df['laser_change'].dropna().apply(lambda x: x == 0).all())

    def test_switch_to_timedeltaindex_first_index_is_0(self):
        l_df = prepare_utility.switch_to_timedelta_index(
            self.read_bagfile(self.__class__._stimulation_bagpath)[0])
        self.assertEqual(l_df.first_valid_index(), pd.Timedelta(seconds=0))

    def test_switch_to_timedeltaindex_last_index_is_last_index_before_minus_first_index_before(self):
        l_df = self.read_bagfile(self.__class__._stimulation_bagpath)[0]
        expected_last_index = l_df.last_valid_index() - l_df.first_valid_index()
        l_df = prepare_utility.switch_to_timedelta_index(l_df)
        self.assertEqual(l_df.last_valid_index(), expected_last_index)

    def test_add_section_column_2_sections(self):
        results = self.read_bagfile(self.__class__._stimulation_bagpath)
        l_df = prepare_utility.switch_to_timedelta_index(results[0])
        geom = results[4]
        l_df = prepare_utility.add_section_column(l_df, geom)
        # print l_df['laser_power'].value_counts()
        self.assertEqual(l_df['section'].max(), 1)

    def test_add_section_column_sections_the_same_time_length(self):
        results = self.read_bagfile(self.__class__._stimulation_bagpath)
        l_df = prepare_utility.switch_to_timedelta_index(results[0])
        geom = results[4]
        l_df = prepare_utility.add_section_column(l_df, geom)

        # auf centi sekunde genau
        l_df = l_df.resample('100L', how='first')
        time_first = (l_df['section'][l_df['section'] == 0].last_valid_index() - l_df[l_df['section'] == 0].first_valid_index())
        time_second = (l_df['section'][l_df['section'] == 1].last_valid_index() - l_df[l_df['section'] == 1].first_valid_index())
        self.assertEqual(time_first, time_second)

    def test_first_laser_on_index_between_2_and_4(self):
        l_df = prepare_utility.switch_to_timedelta_index(
            self.read_bagfile(self.__class__._stimulation_bagpath)[0])
        laser_ons_offs_indices = prepare_utility.get_laser_ons_offs_indices(
            l_df)

        self.assertTrue(pd.Timedelta(seconds=2) <= laser_ons_offs_indices[0][0] <=
                        pd.Timedelta(seconds=4))

    def test_get_laser_on_durations_with_fake_laser_ons_offs_indices_1(self):
        laser_ons_offs_indices = [[pd.Timedelta(seconds=3), pd.Timedelta(seconds=7)],
                                  [pd.Timedelta(seconds=4), pd.Timedelta(seconds=8)]]
        laser_on_durations = prepare_utility.get_laser_on_durations(
            laser_ons_offs_indices)
        self.assertTrue(all([laser_on_duration == pd.Timedelta(seconds=1)
                        for laser_on_duration in laser_on_durations]))

    def test_get_laser_off_durations_with_fake_laser_ons_offs_indices_3(self):
        laser_ons_offs_indices = [[pd.Timedelta(seconds=3), pd.Timedelta(seconds=7)],
                                  [pd.Timedelta(seconds=4), pd.Timedelta(seconds=8)]]
        laser_off_durations = prepare_utility.get_laser_off_durations(laser_ons_offs_indices,
                                                                      pd.Timedelta(seconds=0),
                                                                      pd.Timedelta(seconds=11))

        self.assertTrue(all([laser_off_duration == pd.Timedelta(seconds=3)
                        for laser_off_duration in laser_off_durations]))


if __name__ == '__main__':
    unittest.main()
