'''Plot and info file to test the integrity and some assumptions about the
data before storing them in the hdfstore. All info and plots are saved to one
file.'''

# At this point this is completly optional in
# the pipeline and has to be done manually i.e. run this script after preparing
# the data and remove bagfiles with corrupted/weird data.This will be automated
# at some point but currently I am lacking the intuition as to what amount of
# nan values are to be expected etc.

import argparse
import json
import os
import re

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

import prepare
import prepare_utility

from read_write_utility import get_bagpaths, filter_bagpaths_without_cache


def get_fraction_none_df(df):
    fraction_none_per_column = df.isnull().sum() / float(len(df))
    fraction_none_per_column.name = 'fraction_none'
    fraction_none_per_column.index.name = 'column_name'
    fraction_none_per_column = fraction_none_per_column.reset_index()
    fraction_none_per_column['bagname'] = df.loc[df['bagname'].first_valid_index(), 'bagname']
    return fraction_none_per_column


def get_fraction_of_nan_values_per_column_plot(exp_dfs):
    fraction_none_dfs = map(get_fraction_none_df, exp_dfs)
    fraction_none_all_dfs = pd.concat(fraction_none_dfs).reset_index()

    with sns.plotting_context('poster', font_scale=0.5):
        with mpl.rc_context(rc={'xtick.labelsize': 8}):
            sns.FacetGrid(fraction_none_all_dfs, row='bagname', size=4, aspect=5, subplot_kws=dict(
                ylim=(-0.1, 1))).map(sns.stripplot, 'column_name', 'fraction_none')


def get_protocol(study_name):
    study_name = study_name.replace('point', '.')
    try:
        return re.match(r"[0-9]+\.?[0-9]*\+[0-9]+\.?[0-9]*\+[0-9]+\.?[0-9]*x[0-9]+", study_name).group(0)
    except AttributeError:
        return None


def get_expected_laser_ons(delay, on_duration, off_duration, how_often):
    on_plus_off_duration = on_duration + off_duration
    expected_laser_ons = [delay + on_plus_off_duration *
                          x for x in range(int(how_often))]
    return expected_laser_ons


def get_expected_laser_offs(delay, on_duration, off_duration, how_often):
    on_plus_off_duration = on_duration + off_duration
    expected_laser_offs = [delay + on_duration +
                           on_plus_off_duration * x for x in range(int(how_often))]
    return expected_laser_offs


def get_expected_laser_ons_offs_zerod(protocol):
    # 30+1+30*5
    stimulation_values, how_often = protocol.split('x')
    how_often = int(how_often)
    delay, on_duration, off_duration = [pd.Timedelta(
        seconds=float(x)) for x in stimulation_values.split('+')]

    _expected_laser_ons = get_expected_laser_ons(
        delay, on_duration, off_duration, how_often)
    _expected_laser_offs = get_expected_laser_offs(
        delay, on_duration, off_duration, how_often)

    # we have to zero the first laseronset because there can be a variable time
    # recorded before because of sleep in record file
    zero = _expected_laser_ons[0]
    expected_laser_ons = [expected_laser_on -
                          zero for expected_laser_on in _expected_laser_ons]
    expected_laser_offs = [expected_laser_off -
                           zero for expected_laser_off in _expected_laser_offs]
    return expected_laser_ons, expected_laser_offs


def get_actual_laser_ons_offs_zerod(df):
    # we have to zero the first laseronset because there can be a variable time
    # recorded before because of sleep in record file
    _actual_laser_ons, _actual_laser_offs = prepare_utility.get_laser_ons_offs_indices(
        df)
    if _actual_laser_ons:
        zero = _actual_laser_ons[0]
        actual_laser_ons = [actual_laser_on -
                            zero for actual_laser_on in _actual_laser_ons]
    else:
        actual_laser_ons = [None]

    if _actual_laser_offs:
        actual_laser_offs = [actual_laser_off -
                             zero for actual_laser_off in _actual_laser_offs]
    else:
        actual_laser_offs = [None]

    return actual_laser_ons, actual_laser_offs


def get_in_tile_start(exp_df):
    return exp_df.at[exp_df['in_area'].first_valid_index(), 'in_area']


def get_infos(exp_dfs):
    infos = []

    for exp_df in exp_dfs:
        study_name = exp_df.loc[exp_df['study'].first_valid_index(), 'study']
        protocol = get_protocol(study_name)
        bagname = exp_df.loc[exp_df['bagname'].first_valid_index(), 'bagname']

        if not protocol:
            expected_laser_ons, expected_laser_offs = '?', '?'
        else:
            expected_laser_ons, expected_laser_offs = get_expected_laser_ons_offs_zerod(
                protocol)

        actual_laser_ons, actual_laser_offs = get_actual_laser_ons_offs_zerod(
            exp_df)

        in_tile_start = get_in_tile_start(exp_df)

        info = [('name', bagname),
                ('expected_laser_ons', [str(x) for x in expected_laser_ons]),
                ('actual_laser_ons', [str(x) for x in actual_laser_ons]),
                ('expected_laser_offs', [str(x) for x in expected_laser_offs]),
                ('actual_laser_offs', [str(x) for x in actual_laser_offs]),
                ('in_tile_start'), str(in_tile_start)]

        infos.append(info)

    return infos


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--data', help='path to data folder')

    args = parser.parse_args()
    path = args.data

    if os.path.isdir(path):
        bagpaths = get_bagpaths(path)
        info_directory_path = os.path.join(path, 'validation')
    else:
        bagpaths = [path]
        info_directory_path = os.path.join(os.path.dirname(path), 'info')

    _bagpaths = filter_bagpaths_without_cache(bagpaths)

    if _bagpaths:
        prepare.prepare_multi(_bagpaths)

    exp_dfs = []
    for bagpath in bagpaths:
        exp_df = prepare.prepare(bagpath)
        exp_dfs.append(exp_df)

    if not os.path.isdir(info_directory_path):
        os.makedirs(info_directory_path)

    get_fraction_of_nan_values_per_column_plot(exp_dfs)
    plt.savefig(os.path.join(info_directory_path, 'fraction_none.png'))
    infos = get_infos(exp_dfs)
    with open(os.path.join(info_directory_path, 'info.json'), "w") as info_file:
        json.dump(infos, info_file, indent=4, sort_keys=True)
