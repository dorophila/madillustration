'''This takes bagpaths and returns one resampled dataframe per bagpath with some
values already precomputed: velocity, scalled coordinates, in_area, laser_change,
section etc. '''

import argparse
import multiprocessing
import os.path
import sys

import src.kalman as kalman
from prepare_utility import *
from read_bagfile import read_bagfile
from read_write_utility import (get_bagpaths, load_cache, make_cache_dirs,
                                save_cache, get_cache_path)


def prepare(bagpath):

    cache_path = get_cache_path(bagpath, 'cache')

    make_cache_dirs(bagpath, 'raw_caches')
    make_cache_dirs(bagpath, 'caches')

    results = load_cache(cache_path)

    if results is not None:
        return results

    l_df, t_df, h_df, arena_dict, geom = read_bagfile(bagpath)

    t_df = filter_short_tracking(t_df)

    t_df = add_scaled_coordinates_columns(t_df, arena_dict)
    t_df = add_theta_wrapped_column(t_df)

    kalmantheta = kalman.KalmanTheta(10.0, 1500.0)
    kalmanposition = kalman.KalmanPosition(10.0, 10.0)
    t_df = add_position_smooth_columns(t_df, kalmanposition)
    t_df = add_theta_wrapped_smooth_column(t_df, kalmantheta)
    t_df = add_velocity_column(t_df, x_column='x_smooth', y_column='y_smooth')
    t_df = add_angular_velocity_column(
        t_df, theta_wrapped_column='theta_wrapped_smooth')

    exp_df = concatenate_dfs(l_df, t_df, h_df, arena_dict, geom, bagpath)
    exp_df = switch_to_timedelta_index(exp_df)
    exp_df = resample_10L(exp_df, arena_dict, geom, bagpath)

    exp_df = add_meta_data_columns(exp_df, bagpath)

    exp_df = add_arena_column(exp_df, arena_dict)
    exp_df = add_geom_column(exp_df, geom)
    exp_df = add_in_area_column(exp_df, arena_dict, geom)

    if exp_df['laser_power'].isnull().all():
        exp_df['laser_power'] = 0.0
    exp_df = add_laser_state_column(exp_df)
    exp_df = add_laser_change_column(exp_df)
    exp_df = add_section_column(exp_df, geom)
    exp_df = add_section_column(exp_df, geom, small=True)

    save_cache(exp_df, cache_path)

    return exp_df


def prepare_multi(bagpaths):
    make_cache_dirs(bagpaths, 'raw_caches')
    make_cache_dirs(bagpaths, 'caches')
    p = multiprocessing.Pool()
    p.map(prepare, bagpaths)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', help='path to data folder')

    args = parser.parse_args()
    path = args.data

    if os.path.isdir(path):
        bagpaths = get_bagpaths(path)
    else:
        bagpaths = [path]

    prepare_multi(bagpaths)
