import shutil
import os
import argparse


def remove_dir(dirpath):
    try:
        shutil.rmtree(dirpath)
    except OSError:
        pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('path', nargs=1, help='path to data folder')
    parser.add_argument('--raw-caches', default=False, action='store_true')
    parser.add_argument('--caches', default=False, action='store_true')
    args = parser.parse_args()
    path = args.path[0]

    raw_caches_dir = os.path.join(path, 'raw_caches')
    caches_dir = os.path.join(path, 'caches')

    for root, dirnames, filenames in os.walk(path):
        raw_caches_dir = os.path.join(root, 'raw_caches')
        caches_dir = os.path.join(root, 'caches')

        if args.raw_caches:
            remove_dir(raw_caches_dir)

        if args.caches:
            remove_dir(caches_dir)

        if not args.caches and not args.raw_caches:
            remove_dir(raw_caches_dir)
            remove_dir(caches_dir)
