''' These are functions to get from the raw dfs too one big resampled
df, with some values already computed (velocity, scaled coordinates,
in_area, laser_change, sections etc.) Care needs to be taken
in which order these functions are being used. As mistakes can accumulate/occur
if you resample/fill in between'''

import copy
import datetime
import json
import os
import re
import sys

import numpy as np
import pandas as pd
from shapely.geometry import Point, Polygon

meta_data_columns = ['study', 'binary_genotype', 'genotype', 'flymad_setting',
                     'age', 'info', 'start_string', 'start_time', 'bagname']


def get_velocity(df, x_column, y_column, prefix):
    dt = df.loc[:, [x_column, y_column]].dropna().index.to_series().diff().dt.total_seconds()
    dx = df.loc[:, [x_column, y_column]].dropna()[x_column].diff()
    dy = df.loc[:, [x_column, y_column]].dropna()[y_column].diff()
    df['vx'] = dx / dt
    df['vy'] = dy / dt
    df['{}v'.format(prefix)] = np.sqrt(df['vx']**2 + df['vy']**2)
    df = df.drop(['vx', 'vy'], axis=1)
    return df


def add_velocity_column(df, x_column='x', y_column='y', prefix=''):

    if 'obj_id' in df.columns:
        df = df.groupby('obj_id', group_keys=False).apply(lambda x: get_velocity(x, x_column, y_column, prefix))
    else:
        df = get_velocity(df, x_column, y_column, prefix)

    return df


def get_angular_velocity(df, theta_wrapped_column, prefix):
    dt = df.loc[:, theta_wrapped_column].dropna().index.to_series().diff().dt.total_seconds()
    df['{}angular_velocity'.format(prefix)] = df[theta_wrapped_column].dropna().diff() / dt
    return df


def add_angular_velocity_column(df, theta_wrapped_column='theta_wrapped', prefix=''):

    if 'obj_id' in df.columns:
        df = df.groupby('obj_id', group_keys=False).apply(lambda x:get_angular_velocity(x, theta_wrapped_column, prefix))
    else:
        df = get_angular_velocity(df, theta_wrapped_column, prefix)

    return df


def add_theta_wrapped_smooth_column(df, kalmantheta, theta_column='theta_wrapped'):
    def smooth_theta_wrapped(df):
        first = df.at[df[theta_column].first_valid_index(), theta_column]
        smoothed_theta = kalmantheta.smooth(df[theta_column].values, first=first)
        df['{}_smooth'.format(theta_column)] = smoothed_theta[:, 0]
        return df

    if 'obj_id' in df.columns:
        df = df.groupby('obj_id', group_keys=False).apply(smooth_theta_wrapped)
    else:
        print 'Assuming single obj_id'
        df = smooth_theta_wrapped(df)
    return df


def add_position_smooth_columns(df, kalmanposition, x_column='x', y_column='y'):
    def smooth_positions(df):
        smoothed_positions = kalmanposition.smooth(df[x_column], df[y_column])
        df['{}_smooth'.format(x_column)] = smoothed_positions[:, 0]
        df['{}_smooth'.format(y_column)] = smoothed_positions[:, 1]
        return df

    if 'obj_id' in df.columns:
        df = df.groupby('obj_id', group_keys=False).apply(smooth_positions)
    else:
        print 'Assuming single obj_id'
        df = smooth_positions(df)

    return df


def add_scaled_coordinates_columns(df, arena_dict, raw_x='x_raw', raw_y='y_raw', prefix=''):
    df['{}x'.format(prefix)] = (
        df[raw_x] - arena_dict['cx_raw']) * arena_dict['sx']
    df['{}y'.format(prefix)] = (
        df[raw_y] - arena_dict['cy_raw']) * arena_dict['sy']
    return df


def add_theta_wrapped_column(df, theta_column='theta', prefix=''):
    def unwrap_pi(df):
        df['dtheta'] = df[theta_column].dropna().diff()
        df['dthetamod'] = df['dtheta'].add(np.pi / 2.).mod(np.pi).sub(np.pi/2.)
        df.loc[(df['dthetamod'] == -np.pi/2.) & (df['dtheta'] > 0), 'dthetamod'] = np.pi/2.
        df['ph_correct'] = df['dthetamod'] - df['dtheta']
        df.loc[df['dtheta'].apply(np.abs) < np.pi/2., 'ph_correct'] = 0
        df['{}theta_wrapped'.format(prefix)] = df[theta_column] + df['ph_correct'].cumsum()
        df.drop(['dtheta', 'dthetamod', 'ph_correct'], axis=1)
        return df

    if 'obj_id' in df.columns:
        df = df.groupby('obj_id', group_keys=False).apply(unwrap_pi)
    else:
        print 'Assuming single obj_id'
        df = unwrap_pi(df)

    return df


def filter_short_tracking(df, filter_short=50):
    df = df.groupby('obj_id').filter(lambda x: len(x) > filter_short)
    return df


def add_in_area_column(df, arena_dict, geom):
    if geom:
        df['in_area'] = (df[['x_raw', 'y_raw']].apply(
            lambda row: Polygon(geom).contains(
                Point(row['x_raw'], row['y_raw'])),
            axis=1))
    else:
        # boolean columns with Nones become object dytpe
        # trouble with hdf
        df['in_area'] = False

    return df


def add_laser_state_column(df):
    def get_laser_state(laser_power):
        if laser_power > 0:
            return 1.0
        elif laser_power == 0:
            return 0.0

    df['laser_state'] = df['laser_power'].apply(get_laser_state)
    return df


def add_laser_change_column(df):
    if 'laser_state' not in df.columns:
        df = add_laser_state_column(df)

    # laser was always on
    if df['laser_state'].dropna().apply(lambda x: x == 1).all():
        df['laser_change'] = 0.0
        df.loc[df.first_valid_index(), 'laser_change'] = 1.0
        df.loc[df.last_valid_index(), 'laser_change'] = -1.0
        return df

    # laser was always off
    if df['laser_state'].dropna().apply(lambda x: x == 0).all():
        df['laser_change'] = 0.0
        return df

    df['laser_change'] = df['laser_state'].fillna(
        method='ffill').fillna(method='bfill').diff()

    first_non_zero = df.loc[df['laser_change'] != 0, 'laser_change'].dropna().iloc[0]
    last_non_zero = df.loc[df['laser_change'] != 0, 'laser_change'].dropna().iloc[-1]


    # if first non zero value is -1 the laser was on at the begining
    if first_non_zero == -1.0:
        df.loc[df.first_valid_index(), 'laser_change'] = 1.0

    # if last non zero value is 1 the laser was on at the end
    if last_non_zero == 1.0:
        df.loc[df.last_valid_index(), 'laser_change'] = -1.0

    return df


def add_arena_column(df, arena_dict):
    df['arena'] = json.dumps(arena_dict)
    return df


def add_geom_column(df, geom):
    if geom:
        df['geom'] = json.dumps(geom)
    else:
        df['geom'] = None
    return df


def switch_to_timedelta_index(df, zeroer=None):
    if not zeroer:
        zeroer = df.first_valid_index()
    df.set_index(df.index - zeroer, inplace=True)
    return df


def add_meta_data_columns(df, bagpath):
    def get_metadata_from_bagpath(bagpath):
        study, binary_genotype, genotype, flymad_settings, age, info, start = \
            os.path.basename(bagpath).split('.')[0].split('_')

        age = int(re.search(r'\d+', age).group())
        start_time = datetime.datetime.strptime(
            str(start), "%Y-%m-%d-%H-%M-%S")

        return [('study', study), ('binary_genotype', binary_genotype),
                ('genotype', genotype), ('flymad_setting', flymad_settings),
                ('age', age), ('info', str(info)), ('start_time', start_time),
                ('start_string', start), ('bagname', os.path.basename(bagpath))]

    metadata = get_metadata_from_bagpath(bagpath)
    for column_name, data in metadata:
        df.loc[:, column_name] = data

    return df


def resample_10L(df, arena_dict, geom, bagpath):
    df = df.resample('10L').first()
    df = recompute_constant_columns(df, arena_dict, geom, bagpath)
    df = recompute_laser_edges_columns(df)
    return df


def concatenate_dfs(t_df, l_df, h_df, arena_dict, geom, bagpath):
    df = pd.concat((t_df, l_df, h_df), axis=1)
    df = recompute_constant_columns(df, arena_dict, geom, bagpath)
    return df


def recompute_constant_columns(df, arena_dict, geom, bagpath):
    # some things are supposed to have a value for every measurements
    # instead of tedious filling logics just recompute some values
    if 'arena' in df.columns:
        df = add_arena_column(df, arena_dict)
    if 'geom' in df.columns:
        df = add_geom_column(df, geom)

    if any([column in df.columns for column in meta_data_columns]):
        df = add_meta_data_columns(df, bagpath)

    return df


def recompute_laser_edges_columns(df):
    # laser_change and as a result section can not be resampled in the same
    # way as laser_power/state
    # just recompute
    if 'laser_change' in df.columns:
        df = add_laser_change_column(df)

    if 'section' in df.columns:
        df = add_section_column(df)

    return df


def add_section_column(df, geom, small=False):

    if small:
        column_name = 'small_section'
    else:
        column_name = 'section'

    if geom:
        df[column_name] = np.nan
        return df

    laser_ons_offs_indices = get_laser_ons_offs_indices(df)

    laser_on_durations = get_laser_on_durations(laser_ons_offs_indices)

    # sectioning only makes sense if we have a constant laser on time
    # +/- 1 second for ros issues

    df[column_name] = np.nan

    try:
        if (max(laser_on_durations) - min(laser_on_durations) > datetime.timedelta(
                seconds=1)):
            return df
    except ValueError:
        df[column_name] = 0.0
        return df

    if small:
        section_offset = pd.Timedelta(seconds=1)
    else:
        laser_off_durations = get_laser_off_durations(laser_ons_offs_indices,
                                                      df.first_valid_index(),
                                                      df.last_valid_index())

        section_offset = min(laser_off_durations) / 2.0

    df[column_name] = np.nan
    for section_num, (laser_on, laser_off) in enumerate(zip(
            laser_ons_offs_indices[0], laser_ons_offs_indices[1])):
        df.loc[(laser_on - section_offset):(laser_off + section_offset), column_name] = float(section_num)

    return df


def get_laser_ons_offs_indices(df):

    if 'laser_change' not in df.columns:
        df = add_laser_change_column(df)

    laser_ons_indices = df[df['laser_change'] == 1].index.tolist()
    laser_offs_indices = df[df['laser_change'] == -1].index.tolist()

    laser_ons_offs_indices = (laser_ons_indices, laser_offs_indices)

    return laser_ons_offs_indices


def get_laser_on_durations(laser_ons_offs_indices):
    '''Takes [(laser_on times...), (laser_off times...)] and returns
    [duration of first laser period, duration of second laser period, ... ]]'''

    laser_on_durations = [
        laser_off - laser_on
        for laser_on, laser_off in zip(laser_ons_offs_indices[0],
                                       laser_ons_offs_indices[1])
    ]
    return laser_on_durations


def get_laser_off_durations(laser_ons_offs_indices, beginning, end):
    '''Takes [(laser_on times...), (laser_off times...)] and returns
    [duration before first laser onset, duration between first laser offset
    and second laser onset, ... , duration between last laser offset and end]]
    '''
    _laser_ons_offs_indices = copy.deepcopy(laser_ons_offs_indices)

    # to calculate laser_off periods treat the end of the df as the last laser
    # on
    _laser_ons_offs_indices[0].append(end)

    # to calculate laser_off periods treat the beginning of the dataframe as
    # the first laser off
    _laser_ons_offs_indices[1].insert(0, beginning)

    laser_off_durations = [
        laser_on - laser_off
        for laser_on, laser_off in zip(_laser_ons_offs_indices[0],
                                       _laser_ons_offs_indices[1])
    ]

    return laser_off_durations
