'''
Creates the h5store
'''

import argparse
import os
import sys

import pandas as pd
import prepare
from read_write_utility import get_bagpaths, filter_bagpaths_without_cache


def store_data(bagpath, store_path, mode='a'):
    dataframe = prepare.prepare(bagpath)
    start_string_this = dataframe.at[
        dataframe['start_string'].first_valid_index(), 'start_string']

    with pd.HDFStore(store_path, mode=mode) as store:

        if store.keys():
            if not store.select('data', "start_string == start_string_this").empty:
                print 'experiment {} already in store'.format(start_string_this)
                return

        print 'writting experiment {} to store'.format(start_string_this)
        # timedeltaindex to store is buggy
        dataframe['timedelta'] = dataframe.index
        store.append('data',
                     dataframe,
                     data_columns=True,
                     min_itemsize={
                         'genotype': 40,
                         'binary_genotype': 4,
                         'info': 100,
                         'flymad_setting': 60,
                         'geom': 100,
                         'arena': 150,
                         'bagname': 200,
                         'study': 40,
                         'age': 40
                     })


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', help='path to data folder')
    parser.add_argument('--store', default=False, dest='store_path',
                        help='path of the store to put the data in')

    args = parser.parse_args()
    path = args.data

    if args.store_path:
        # storepath is given: create or append the store
        store_path = args.store_path
    else:
        # no storename create or append to store.h5 in the data directory
        store_path = os.path.join(os.path.dirname(path), 'store.h5')

    if os.path.isdir(path):
        bagpaths = get_bagpaths(path)
    else:
        bagpaths = [path]

    _bagpaths = filter_bagpaths_without_cache(bagpaths)

    if _bagpaths:
        prepare.prepare_multi(_bagpaths)

    for bagpath in bagpaths:
        store_data(bagpath, store_path)
