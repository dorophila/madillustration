import numpy as np
import adskalman.adskalman


class Kalman(object):

    def __init__(self, Qsigma, Rsigma):
        self.Qsigma = Qsigma
        self.Rsigma = Rsigma
        FPS = 100
        self.dt = 1.0/FPS


class KalmanPosition(Kalman):

    def __init__(self, Qsigma, Rsigma):
        super(KalmanPosition, self).__init__(Qsigma, Rsigma)

        # process model
        self.A = np.array([[1, 0, self.dt, 0],
                          [0, 1, 0, self.dt],
                          [0, 0, 1,  0],
                          [0, 0, 0,  1]],
                          dtype=np.float64)
        # observation model
        self.C = np.array([[1, 0, 0, 0],
                           [0, 1, 0, 0]],
                          dtype=np.float64)
        # process covariance
        self.Q = self.Qsigma*np.eye(4)
        # measurement covariance
        self.R = self.Rsigma*np.eye(2)

    def smooth(self, x, y):

        y = np.c_[x, y]
        initx = np.array([y[0, 0], y[0, 1], 0, 0])
        initV = 0*np.eye(4)

        xsmooth, Vsmooth = adskalman.adskalman.kalman_smoother(y,
                                                               self.A, self.C,
                                                               self.Q, self.R,
                                                               initx, initV)

        return xsmooth


class KalmanTheta(Kalman):

    def __init__(self, Qsigma, Rsigma):
        super(KalmanTheta, self).__init__(Qsigma, Rsigma)

        # process model
        self.A = np.array([[1, self.dt],
                          [0, 1]],
                          dtype=np.float64)

        # observation model
        self.C = np.array([[1, 0]],
                          dtype=np.float64)

        # process covariance
        self.Q = self.Qsigma*np.eye(2)
        # measurement covariance
        self.R = self.Rsigma*np.eye(1)

    def smooth(self, theta, first=None):

        if not first:
            initx = np.array([theta[0], 0])
        else:
            initx = np.array([first, 0])

        initV = 0*np.eye(2)

        xsmooth, Vsmooth = adskalman.adskalman.kalman_smoother(theta,
                                                               self.A, self.C,
                                                               self.Q, self.R,
                                                               initx, initV)

        return xsmooth
